FROM node:18-slim

RUN apt-get update && \
	apt-get install -y samtools tabix \
	&& rm -rf /var/lib/apt/lists/*

RUN npm install -g @jbrowse/cli && jbrowse create /web

# RUN groupadd -g 10001 web && \
#    useradd -u 10000 -g web web \
#    && chown -R web:web /web
# 
# 
# USER 10000:10001
WORKDIR /web
